using System;
using Moq;
using NUnit.Framework;
using UniRx;

namespace Editor
{
    public class BoycottCongressmanShould
    {
        private BoycottCongressman _boycottCongressman;
        private Mock<ICongressmanView> _view;
        private const int ID = 0;
        
        private const int SHORT_LAW_LENGTH = 5;

        [SetUp]
        public void SetUp()
        {
            _view = new Mock<ICongressmanView>();
            _boycottCongressman = new BoycottCongressman(_view.Object,ID);
        }

        [Test]
        public void Throw_Exception_On_Vote()
        {
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);

            ThenExceptionIsThrown(() => WhenVotingALaw(law));
        }

        [Test]
        public void Prevent_Log_On_Boycott()
        {
             var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
             WhenBoycott(law);
             ThenVoteIsNotLogged();
        }

        private void WhenVotingALaw(Law law)
        {
           _boycottCongressman.ReceiveLaw(law).Subscribe();
        }

        private void WhenBoycott(Law law)
        {
            ThenExceptionIsThrown(() => WhenVotingALaw(law));
        }

        private void ThenExceptionIsThrown(Action OnVote)
        {
            Assert.Throws<BoycottException>(() => OnVote());
        }

        private void ThenVoteIsNotLogged()
        {
            _view.Verify(view => view.LogVote(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<Vote>()), Times.Never);
        }
    }

    public class BoycottCongressman : Congressman
    {
        public override IObservable<Vote> ReceiveLaw(Law law)
        {
            return Observable.Throw<Vote>(new BoycottException());
        }

        public BoycottCongressman(ICongressmanView view, int id) : base(view, id)
        {
        }
    }

    public class BoycottException : Exception
    {
    }
}