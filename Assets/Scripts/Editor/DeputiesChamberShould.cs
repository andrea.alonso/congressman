﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UniRx;
using UnityEngine;

namespace Editor
{
    public class DeputiesChamberShould 
    {
        private DeputiesChamber _deputiesChamber;
        private List<Congressman> _congressmen;
        private TestScheduler _scheduler;
        private Mock<IDeputiesChamberView> _view;
        private const int START_VOTING_TIME = 0;

        private const int CONGRESSMAN_AMOUNT = 10;
        private const int SHORT_LAW_TIME = 501;
        private const int LONG_LAW_TIME = 20001;
        private const int EMPTY_LAW_LENGTH= 0;
        private const int SHORT_LAW_LENGTH = 5;
        private const int LONG_LAW_LENGTH = 20;

        [SetUp]
        public void SetUp()
        {
            _scheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.TimeBasedOperations = _scheduler;
            _congressmen = new List<Congressman>();
            
            for (int i = 0; i < CONGRESSMAN_AMOUNT; i++)
                _congressmen.Add(new Congressman(new Mock<ICongressmanView>().Object, i));
            
            _view = new Mock<IDeputiesChamberView>();
            _view.Setup(view => view.LogStartSession(It.IsAny<Law>()))
                .Returns((Law lawType) => Observable.Return(lawType));
            
            _view.Setup(view => view.LogEndSession(It.IsAny<Law>()))
                .Returns((Law lawType) => Observable.Return(lawType));
            _deputiesChamber = new DeputiesChamber(_view.Object,_congressmen);
        }
    
        [Test]
        public void Approve_Law_On_Majority_Votes()
        {
            LawState lawIsApproved = LawState.Default;
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
            lawIsApproved = WhenVotingForLaw(law, SHORT_LAW_TIME);
            ThenLawIsApproved(lawIsApproved);
        }

        [Test]
        public void Reject_Law_On_Minority_Votes()
        {
            LawState lawState = LawState.Default;
            var law = new Law(LawType.Long,LONG_LAW_LENGTH);
            lawState = WhenVotingForLaw(law, LONG_LAW_TIME);
            ThenLawIsRejected(lawState);
        }

        [Test]
        public void Reject_Law_On_Boycott()
        {
            _congressmen.Add(new BoycottCongressman(new CongressmanView(),CONGRESSMAN_AMOUNT));
            
            LawState lawIsApproved = LawState.Default;
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
            lawIsApproved = WhenVotingForLaw(law, SHORT_LAW_TIME);
            ThenLawIsRejected(lawIsApproved); 
        }

        [Test]
        public void Log_Law_On_Start_Voting()
        {
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
            WhenVotingForLaw(law, START_VOTING_TIME);
            ThenStartSessionIsLogged(law);
        }
        
        [Test]
        public void Log_Law_On_End_Voting()
        {
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
            WhenVotingForLaw(law, SHORT_LAW_TIME);
            ThenEndSessionIsLogged(law);
        }


        private LawState WhenVotingForLaw(Law law, int time)
        {
            LawState lawIsApproved = LawState.Default;
            _deputiesChamber.ApproveLaw(law)
                .Do(v => lawIsApproved = v)
                .Subscribe();
            _scheduler.AdvanceBy(TimeSpan.FromMilliseconds(time).Ticks);
            return lawIsApproved;
        }

        private void ThenLawIsRejected(LawState lawIsApproved)
        {
            Assert.AreEqual(LawState.Rejected,lawIsApproved);
        }

        private static void ThenLawIsApproved(LawState lawIsApproved)
        {
            Assert.AreEqual(LawState.Approved,lawIsApproved);
        }

        private void ThenStartSessionIsLogged(Law law)
        {
            _view.Verify(view => view.LogStartSession(law), Times.Once);
        }

        private void ThenEndSessionIsLogged(Law law)
        {
            _view.Verify(view => view.LogEndSession(law), Times.Once);
        }
    }

    public interface IDeputiesChamberView
    {
        IObservable<Law> LogStartSession(Law law);
        IObservable<Law> LogEndSession(Law law);
    }

    public enum LawState
    {
        Default,
        Approved,
        Rejected
    }

    public class DeputiesChamber
    {
        private readonly IDeputiesChamberView _view;
        private readonly List<Congressman> _congressmen;

        public DeputiesChamber(IDeputiesChamberView view, List<Congressman> congressmen)
        {
            _view = view;
            _congressmen = congressmen;
        }

        public IObservable<LawState> ApproveLaw(Law law)
        {
            int voteCount = 0;
            
            return  _view.LogStartSession(law).ContinueWith(_congressmen.ToObservable()
                .SelectMany(x => x.ReceiveLaw(law))
                .Do(onNext: vote =>
                {
                    if(vote == Vote.Approved) 
                        voteCount++;
                })
                .Concat()
                .Select(x =>
                {
                    if (voteCount > _congressmen.Count / 2)
                        return LawState.Approved;
                    return LawState.Rejected;
                }).Catch( (BoycottException x) => Observable.Return(LawState.Rejected) ).Last()).DoOnCompleted(()=>_view.LogEndSession(law));
        }


        private bool IsLawApproved(Law law)
        {
            return  _congressmen.Select(x => x.ReceiveLaw(law)).Aggregate(0, (accum, current) =>
            {
                current.Do(vote => accum += vote == Vote.Approved ? 1 : 0).Subscribe();
                return accum;
            }) >= _congressmen.Count / 2;
        }
    }
}