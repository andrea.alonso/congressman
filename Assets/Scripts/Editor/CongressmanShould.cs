﻿using System;
using Moq;
using NUnit.Framework;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Editor
{
    public class CongressmanShould
    {
        private Congressman _congressman;
        private TestScheduler _scheduler;
        private Mock<ICongressmanView> _view;
        private const int NO_TIME_ELAPSED =0;
        private const int LESS_THAN_SHORT_TIME = 200;
        private const int EMPTY_LAW_TIME = 1;
        private const int SHORT_LAW_TIME = 501;
        private const int LONG_LAW_TIME = 2001;
        
        
        private const int EMPTY_LAW_LENGTH= 0;
        private const int SHORT_LAW_LENGTH = 5;
        private const int LONG_LAW_LENGTH = 20;
        
        private const int ID = 0;

        [SetUp]
        public void SetUp()
        {
            _scheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.TimeBasedOperations = _scheduler;
            _view = new Mock<ICongressmanView>();
            _congressman = new Congressman(_view.Object, ID);
        }

        [Test]
        public void Approved_Vote_On_Short_Law()
        {
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
            var voteResult = WhenVotingForLaw(law,SHORT_LAW_TIME);
            ThenVoteIsEqualTo(Vote.Approved,voteResult);
        }

        [Test]
        public void Prevent_Vote_Before_Reading_Short_Law()
        {
            var law = new Law(LawType.Short,SHORT_LAW_LENGTH);
            var voteResult = WhenVotingForLaw(law,LESS_THAN_SHORT_TIME);
            ThenVoteIsEqualTo(Vote.None,voteResult);
        }

        [Test]
        public void Rejected_Vote_On_Long_Law()
        {
            var law = new Law(LawType.Long,LONG_LAW_LENGTH);
            var voteResult = WhenVotingForLaw(law,LONG_LAW_TIME);
            ThenVoteIsEqualTo(Vote.Rejected,voteResult);
        }

        [Test]
        public void Prevent_Vote_Before_Reading_Long_Law()
        {
            var law = new Law(LawType.Long,LONG_LAW_LENGTH);
            var voteResult = WhenVotingForLaw(law,SHORT_LAW_TIME);
            ThenVoteIsEqualTo(Vote.None,voteResult);
        }

        [Test]
        public void Abstain_Vote_On_Empty_Law()
        {
            var law = new Law(LawType.Empty,EMPTY_LAW_LENGTH);
            var voteResult = WhenVotingForLaw(law, EMPTY_LAW_TIME);
            ThenVoteIsEqualTo(Vote.Abstain,voteResult);
        }

        [Test]
        public void Prevent_Vote_Before_Reading_Empty_Law()
        {
            var law = new Law(LawType.Empty,EMPTY_LAW_LENGTH);
            var voteResult = WhenVotingForLaw(law,NO_TIME_ELAPSED);
            ThenVoteIsEqualTo(Vote.None,voteResult);
        }

        [TestCase(LawType.Short,SHORT_LAW_LENGTH,SHORT_LAW_TIME,Vote.Approved)]
        [TestCase(LawType.Long,LONG_LAW_LENGTH,LONG_LAW_TIME,Vote.Rejected)]
        [TestCase(LawType.Empty,EMPTY_LAW_LENGTH,EMPTY_LAW_TIME,Vote.Abstain)]
        public void Log_Vote_On_Long_Law_Received(LawType lawType,int lawLength, int time, Vote expectedVote)
        {
            var law = new Law(lawType,lawLength);
            var voteResult = WhenVotingForLaw(law, time);
            ThenVoteIsLogged(expectedVote);
        }

        private IObservable<Vote> WhenReceivingLaw(Law law)
        {
            return _congressman.ReceiveLaw(law);
        }

        private Vote WhenVotingForLaw(Law law, int timeToReadLaw)
        {
            Vote voteResult = Vote.None;
            WhenReceivingLaw(law).Do(vote => voteResult = vote).Subscribe();
            _scheduler.AdvanceBy(TimeSpan.FromMilliseconds(timeToReadLaw).Ticks);
            return voteResult;
        }

        private void ThenVoteIsEqualTo(Vote expectedVote, Vote vote)
        {
             Assert.AreEqual(expectedVote, vote);
        }

        private void ThenVoteIsLogged(Vote expectedVote)
        {
            _view.Verify(view => view.LogVote(ID, _congressman.GetType().ToString(), expectedVote),Times.Once);
        }
    }

    public class Congressman
    {
        private ICongressmanView _view;
        private int _id;

        public Congressman(ICongressmanView view, int id)
        {
            _view = view;
            _id = id;
        }

        protected int timeForCharacter = 100;

        private Vote GetVote(LawType lawType)
        {
            if (lawType == LawType.Short)
                return Vote.Approved;
            if (lawType == LawType.Long)
                return Vote.Rejected;
            if (lawType == LawType.Empty)
                return Vote.Abstain;
            return Vote.None;
        }

        public virtual IObservable<Vote> ReceiveLaw(Law law)
        {
            return PrepareToVote(law).ContinueWith(Observable.Create<Vote>(x =>
            {
                x.OnNext(GetVote(law.GetLawType()));
                x.OnCompleted();
                return Disposable.Empty;
            })).Do(LogVote);
        }

        protected void LogVote(Vote vote)
        {
            _view.LogVote(_id, GetType().ToString(),vote);
        }

        protected virtual IObservable<long> PrepareToVote(Law law)
        {
            return Observable.Timer(CalculateTimeToRead(law));
        }

        private TimeSpan CalculateTimeToRead(Law law)
        {
            return TimeSpan.FromMilliseconds(timeForCharacter * law.GetLength());
        }
    }

    public interface ICongressmanView
    {
        void LogVote(int id, string type, Vote vote);
    }
    
    public class CongressmanView : ICongressmanView
    {
        public void LogVote(int id, string type, Vote vote)
        {
            Debug.Log(id + " " + type + " " + vote);
        }
    }
    
    public class Law
    {
        private readonly LawType _lawType;
        private int _length;

        public Law(LawType lawType,int length)
        {
            _lawType = lawType;
            _length = length;
        }

        public int GetLength()
        {

            return _length;
        }

        public LawType GetLawType()
        {
            return _lawType;
        }
    }

    public enum LawType
    {
        None,
        Short,
        Long,
        Empty
    }

    public enum Vote
    {
        None,
        Approved,
        Rejected,
        Abstain
    }
}